<!-- Footer starts -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
            <p class="copy">Copyright &copy; 2019 | <a href="#"></a> </p>
      </div>
    </div>
  </div>
</footer>

<!-- Footer ends -->
<link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet" />
<style>
div.dt-buttons{
  position: absolute;
  float: right;
  top: 5px;
  right: 22px;

}
div.dataTables_filter {
  top: 12px;
  right: 153px;
}
.ch2{
  float: right;
}
.dataTables_paginate a.current{
  color: #777;
  background: #eee;
}
</style>
<!-- JS -->
<script src="<?php echo $PUBLIC_FILE_URL;?>js/jquery.js"></script> <!-- jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.min.js"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
<script src="<?php echo $PUBLIC_FILE_URL;?>js/bootstrap.js"></script> <!-- Bootstrap -->
<Script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
</body>
</html>
