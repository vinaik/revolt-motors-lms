<?php
$BASE_URL=BASE_URL;
$PUBLIC_FILE_URL=$BASE_URL.'public/';
$PUBLIC_FILE_URL=$BASE_URL.'public/';

?>
<!DOCTYPE html>
<html lang="en">


<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <title>Revolt Motors</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <link href="<?php echo $PUBLIC_FILE_URL;?>css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $PUBLIC_FILE_URL;?>css/font-awesome.css">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >
  <link rel="stylesheet" href="<?php echo $PUBLIC_FILE_URL;?>css/dataTables.bootstrap.css">
  <!-- Main stylesheet -->
  <link href="<?php echo $PUBLIC_FILE_URL;?>css/style.css" rel="stylesheet">

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?php echo $PUBLIC_FILE_URL;?>images/favicon.ico">
  <style>.navbar {  height:60px;} div.dataTables_filter{    top: -41px !important; right: 13px !important;}
  .button_align{
    text-align: right;width: calc(100% - 240px);
  }
  .mt_20{
    margin-top: 20px;
  }
  .mt_30{
    margin-top: 30px;
  }
  .mt_45{
    margin-top: 45px;
  }
  .error{
    color: red;
  }
  th, td{vertical-align: middle;}
  </style>
</head>

<body>
  <header>
    <div class="navbar navbar-fixed-top bs-docs-nav" role="banner">

      <div class="container">
        <div class="navbar-header">
          <a href="#" class="navbar-brand"><span class="bold"></span>Revolt Motors</a>
        </div>
      </div>
    </div>
  </header>


  <div class="content">
    <?php include_once('nav.php'); ?>
