<!-- Sidebar -->
<div class="sidebar" style="width:160px;">
  <div class="sidebar-dropdown"><a href="#">Navigation</a></div>
  <ul id="nav">
    <!-- Main menu with font awesome icon -->
    <li><a href="<?php echo $BASE_URL;?>admin/leads">Unpaid User List</a></li>
    <li><a href="<?php echo $BASE_URL;?>admin/booking-users">Booking Management</a></li>
    <li><a href="<?php echo $BASE_URL;?>admin/logout">Logout</a></li>
  </ul>
</div>
<!-- Sidebar ends -->
