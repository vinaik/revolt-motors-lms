<?php include('include/header.php'); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<div class="mainbar">
	<div class="matter">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="widget">
						<div class="widget-head">
							<div class="col-md-12">
								<div class="">Booking Users</div>
								<div class="widget-icons pull-right">
									<a href="#" class="wminimize"></a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-5">
									<form id="search-form">
										<div class="form-group col-md-6">
											<input name="text_search" type="text" placeholder="Enter Mobile No." class="form-control">
										</div>
										<div class="col-md-3">
											<input type="hidden" name="page" value="1" id="page">
											<input type="submit" value="Search" class="btn btn-primary pull-left btn-block" id="submit_search_btn">
										</div>
										<div class="col-md-3">
											<input type="reset" value="Reset" class="btn btn-primary pull-left btn-block" id="reset_btn">
										</div>

									</form>
								</div>
								<div class="col-md-7">
									<form >
										<div class="form-group col-md-3">
											<select class="form-control" id="city">
												<option value="">Select City</option>
												<option value="delhi">Delhi</option>
												<option value="pune">Pune</option>
												<option value="other">Other</option>
											</select>
										</div>
										<div class="form-group col-md-3">
											<select class="form-control" id="bike_color">
												<option value="">Select Bike Color</option>
												<option value="rebel-red">Rebel Red</option>
												<option value="cosmic-black">Cosmic Black</option>
											</select>
										</div>
										<div class="form-group col-md-3">
											<select class="form-control" id="rv_code">
												<option value="">Filter using RV Code</option>
												<option value="rv-registration">Registration with RV Code</option>
												<option value="rv-payment">Payment with RV Code</option>
											</select>
										</div>
										<div class="col-md-3">
											<input type="hidden" name="page" value="1" id="page">
											<input type="button" value="Download CSV" id="submit_download_csv" class="btn btn-primary pull-left btn-block">
										</div>
									</form>
								</div>
							</div>
						</div>

						<div class="widget-content" style="margin: 0px;width:100%;">

							<div class="table-responsive">
								<table id="datatable" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Id</th>
											<th>Name</th>
											<th>Mobile</th>
											<th>Email</th>
											<th>City</th>
											<th>Registration No.</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
							<div class="widget-foot">
								<br><br>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
</div>
<?php include('include/footer.php'); ?>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var page	=	1;
function checkNotNull(data) {
	return (data!=null && data!='' ? data : "");
}
function setFormat($number){
	return $number < 10 ? '0'+$number:$number;
}
function prevbutton($current_page, href){

	var $prev = $current_page;
	if($current_page > 1){
		var $prev = parseInt($current_page-1);
	}
	return '<li><span class="page-tit">PAGE</span><a href="'+href+'" page="'+$prev+'"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>';
}
function nextbutton($current_page, href, $lastPage){

	var $next = parseInt($current_page)+1;

	if($next > $lastPage)
	$next = parseInt($lastPage);

	return '<li><a href="'+href+'" page="'+$next+'"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>';
}
function fullPagination($current_page, pagination, href, fullnav){
	var html = '';
	var lastpage = '';
	if(pagination){
		html+='<ul class="pagination" style="position:absolute;right:10px;bottom:10px;">';
		if(fullnav)
		html+=prevbutton(page,href);

		$.each(pagination, function(key,value){
			if(value == page) {
				html+='<li class="active"><span class="current">'+value+'</span></li>';
			}else if(value=="break"){
				html+='<li><a href="javascript:;" >......</a></li>';
			}
			else {
				html+='<li><a href="'+href+'" page="'+value+'">'+value+'</a></li>';
			}
			lastPage = value;

		});
		if(fullnav)
		html+=nextbutton(page,href,lastPage);

		html+='</ul>';
	}
	return html;
}
function LoadData(href){

	$.ajax({
		url: href,
		method:"post",
		data:$('#search-form').serialize(),
		success: function(result){
			var res     =   JSON.parse(result);
			var html    =   '';
			$data_exist =   0;
			if(res){
				$.each(res, function(key,value) {

					if(value.id){
						$data_exist++;
						html += '<tr><td>'+ value.id +'</td><td>'+ value.name + '</td><td>'+ value.email +'</td><td>'+ value.mobile + '</td><td>'+ value.city + '</td><td>'+ value.created_date +'</td></tr>';
					}

				});
				if(res.pagination.length){
					html+='<tr><td colspan="5">';

					html+=fullPagination(page,res.pagination,href='<?php echo $BASE_URL;?>admin/allBookingsleads',true);

					html+='</td></tr>';

				}
				$('#datatable').find('tbody').html(html);

			}
			if($data_exist=='0'){
				$('#datatable').find('tbody').html("<tr><td colspan='7'><h4 style='text-align:center;'>No Record Found!</h4></td></tr>");
			}

		}

	});

}
$(document).ready(function(){
	var href    =   "<?php echo $BASE_URL;?>admin/allBookingsleads";
	page	=	1;
	LoadData(href);
});

$(document).on('click','.pagination a',function(e){
	e.preventDefault();
	var href    =   $(this).attr('href');
	page    =   $(this).attr('page');
	$('#page').val(page);
	LoadData(href);
});
$(document).on('click','#submit_search_btn',function(e){
	e.preventDefault();
	page	=	1;
	$('#page').val('1');
	var href    =   "<?php echo $BASE_URL;?>admin/allBookingsleads";
	LoadData(href);
});

$(document).on('click','#submit_download_csv',function(e){
	var city = $('#city').val();
	var bike_color = $("#bike_color").val();
	var rv_code = $("#rv_code").val();
	document.location.href = '<?php echo $BASE_URL;?>admin/total-bookings?city='+city+'&bike_color='+bike_color+'&rv_code='+rv_code;
});

$(document).on('click','#reset_btn',function(e){
	setTimeout(function(){
		var href    =   "<?php echo $BASE_URL;?>admin/allBookingsleads";
		page	=	1;
		LoadData(href);
	}, 1000);


});

$(document).on('click','#add_buses',function(){
	$('#busModal').modal('show');
});

$(document).on('click','#add_more',function(){
	var length  =   $('.delete-section').length;
	var clonediv    =   $('.clone-div').clone(true);
	$(clonediv).removeClass('clone-div');
	$(clonediv).find("input[type='text']").attr("name","bus_no["+length+"]");
	$(clonediv).find("input[type='text']").val('');
	$(clonediv).find('.delete-section').html('<a href="javascript:;" class="delete-row btn btn-danger" >Delete</a>');
	$('.bus_nos').append(clonediv);

});

$(document).on('click','.delete-row',function(){
	$(this).parent().parent().remove();
});

$(document).on('click','#submit_btn',function(){
	$('#add_buses_form').validate();
	if($('#add_buses_form').valid()){
		$('#add_buses_form').submit();
	}

});

</script>
