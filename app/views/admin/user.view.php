<?php include('include/header.php'); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<div class="mainbar">
	<div class="matter">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="widget">
						<div class="widget-head">
							<div class="pull-left">User List</div>
							<div class="widget-icons pull-right">
								<a href="#" class="wminimize"></a>
							</div>
							<div class="clearfix"></div>
							<div class="button_align_new">
								<form id="search-form" class="row">
									<div class="form-group col-md-3">
										<input name="text_search" type="text" placeholder="Enter Mobile No." class="form-control">
									</div>
									<!-- <div class="form-group col-md-3">
										<input id="date-range" name="date-range" type="text" placeholder="Enter date range" class="form-control" value="">
									</div> -->
									<div class="col-md-1">
										<input type="hidden" name="page" value="1" id="page">
										<input type="submit" value="Search" class="btn btn-primary pull-left" id="submit_search_btn">
									</div>
									<div class="col-md-1">
										<input type="reset" value="Reset" class="btn btn-primary pull-left" id="reset_btn">
									</div>
									<div class="col-md-2">
									<a class="btn btn-primary ch2 download_csv" href="<?php echo $BASE_URL;?>admin/downloadLeadscsv">Download CSV</a>

									</div>
									<div class="clearfix"></div>
								</form>

							</div>
							<div class="clearfix"></div>
						</div>

						<div class="widget-content" style="margin: 0px;width:100%;">
							<div class="table-responsive">
								<table id="datatable" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Id</th>
											<th>Name</th>
											<th>Email</th>
											<th>Mobile</th>
											<th>City</th>
											<th>Register Date</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							<div class="widget-foot">
								<br><br>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
</div>

<?php include('include/footer.php'); ?>
<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-date-range-picker/0.20.0/jquery.daterangepicker.min.js"></script>
<script>
var page	=	1;
$("#date-range").daterangepicker();
$("#date-range").val('');
function checkNotNull(data) {
  return (data!=null && data!='' ? data : "");
}
function setFormat($number){
	return $number < 10 ? '0'+$number:$number;
}
function prevbutton($current_page, href){

	var $prev = $current_page;
	if($current_page > 1){
		var $prev = parseInt($current_page-1);
	}
	return '<li><span class="page-tit">PAGE</span><a href="'+href+'" page="'+$prev+'"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>';
}
function nextbutton($current_page, href, $lastPage){

	var $next = parseInt($current_page)+1;

	if($next > $lastPage)
		$next = parseInt($lastPage);

	return '<li><a href="'+href+'" page="'+$next+'"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>';
}
function fullPagination($current_page, pagination, href, fullnav){
	var html = '';
	var lastpage = '';
	if(pagination){
		html+='<ul class="pagination" style="position:absolute;right:10px;bottom:10px;">';
		if(fullnav)
			html+=prevbutton(page,href);

			$.each(pagination, function(key,value){
				if(value == page) {
					html+='<li class="active"><span class="current">'+value+'</span></li>';
				}else if(value=="break"){
					html+='<li><a href="javascript:;" >......</a></li>';
				}
				else {
					html+='<li><a href="'+href+'" page="'+value+'">'+value+'</a></li>';
				}
				lastPage = value;

			});
		if(fullnav)
			html+=nextbutton(page,href,lastPage);

		html+='</ul>';
	}
	return html;
}
function LoadData(href){

	$.ajax({
		url: href,
		method:"post",
		data:$('#search-form').serialize(),
		success: function(result){
			var res     =   JSON.parse(result);
			var html    =   '';
			$data_exist =   0;
			if(res){
				$.each(res, function(key,value) {

					if(value.id){
						$data_exist++;
						html += '<tr><td>'+ value.id +'</td><td>'+ value.name + '</td><td>'+ value.email +'</td><td>'+ value.mobile + '</td><td>'+ value.city + '</td><td>'+ value.register_date +'</td></tr>';
					}

				});
				if(res.pagination.length){
					html+='<tr><td colspan="5">';

					html+=fullPagination(page,res.pagination,href='<?php echo $BASE_URL;?>admin/allleads',true);

					html+='</td></tr>';

				}
				$('#datatable').find('tbody').html(html);

			}
			if($data_exist=='0'){
				$('#datatable').find('tbody').html("<tr><td colspan='7'><h4 style='text-align:center;'>No Record Found!</h4></td></tr>");
			}

		}

	});

}
$(document).ready(function(){
	var href    =   "<?php echo $BASE_URL;?>admin/allleads";
	page	=	1;
	LoadData(href);
});

$(document).on('click','.pagination a',function(e){
	e.preventDefault();
	var href    =   $(this).attr('href');
	page    =   $(this).attr('page');
	$('#page').val(page);
	LoadData(href);
});
$(document).on('click','#submit_search_btn',function(e){
	e.preventDefault();
	page	=	1;
	$('#page').val('1');
	var href    =   "<?php echo $BASE_URL;?>admin/allleads";
	LoadData(href);
});


$(document).on('click','#reset_btn',function(e){
	setTimeout(function(){
		var href    =   "<?php echo $BASE_URL;?>admin/allleads";
		page	=	1;
		LoadData(href);
	}, 1000);


});
</script>
