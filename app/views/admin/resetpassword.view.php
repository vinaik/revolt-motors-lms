<?php
$BASE_URL=BASE_URL;
$PUBLIC_FILE_URL=$BASE_URL.'public/';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <title>Tower Transit LMS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="<?php echo $PUBLIC_FILE_URL;?>css/bootstrap.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $PUBLIC_FILE_URL;?>css/font-awesome.css">
  <link href="<?php echo $PUBLIC_FILE_URL;?>css/style.css" rel="stylesheet">

  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->
  <link rel="shortcut icon" href="<?php echo $PUBLIC_FILE_URL; ?>css/img/favicon/favicon.png">
</head>
<body>
  <!-- Form area -->
  <div class="admin-form">
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <!-- Widget starts -->
          <div class="widget">
            <!-- Widget head -->
            <div class="widget-head">
              <i class="icon-lock"></i> Reset Password
            </div>
            <div class="widget-content">
              <div class="padd">
                <span style="color:red;"><?php if(isset($msg) && !empty($msg)){ echo $msg; $msg=''; } ?></span>
                <!-- Login form -->
                <form class="form-horizontal" action='' method='post'>
                    <input type="hidden" value="<?php echo $id; ?>" name="user_id">
                  <!-- Email -->
                  <div class="form-group">
                    <label class="control-label col-lg-4" for="inputEmail">New Password</label>
                    <div class="col-lg-7">
                      <input type="password" name="password" class="form-control" id="password" placeholder="Enetr your new password" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-lg-4" for="inputEmail">Re-type Password</label>
                    <div class="col-lg-7">
                      <input type="password" name="new-password" class="form-control" id="confirm_password" placeholder="Re-type password" required>
                    </div>
                  </div>
                  <div class="col-lg-9 col-lg-offset-3">
                    <button type="submit" class="btn btn-danger btn-lg" style="margin-left:-10px;">SUBMIT</button>
                  </div>
                  <br />
                  <br />
                </form>

                <div class="clearfix"></div>
              </div>
            </div>

            <div class="widget-foot">
              <center><font color="red" size="2px"></font></center>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- JS -->
  <script src="<?php echo $PUBLIC_FILE_URL; ?>js/bootstrap.js"></script>
</body>
<script>
    var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>
</html>
