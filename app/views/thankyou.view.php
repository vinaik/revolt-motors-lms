<?php include('include/header.php'); 

?>

<section class="gradient-bg thanku-page">
      <div class="container-fluid">
        <div class="logo">
          <a href="">
            <img src="<?php echo $PUBLIC_FILE_URL;?>img/logo.png" alt="">
          </a>
        </div>
        <div class="thanku-msg">
          <h1>Thank you</h1>
        </div>
        <div class="thank-footer">
          <div class=" container">
                <div class="footer">
                   <div class="copy-right">
                     <p>© 2019 Tower Transit Singapore</p>
                   </div>
                   <div class="social-link">
                     <a href="https://www.facebook.com/towertransitsg/" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                     <a href="https://twitter.com/towertransitSG" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                     <a href="https://www.instagram.com/towertransitsg/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                     <p>@TowerTransitSG</p>
                   </div>
                </div>
          </div>
        </div>
      </div>
    </section>
<?php include('include/footer.php'); ?>
