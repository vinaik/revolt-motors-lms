<?php 
$BASE_URL = BASE_URL;
$PUBLIC_FILE_URL = $BASE_URL.'public/';

?>
<!DOCTYPE html>
<html>

<head>
    <title>Tower Transit - Bus Rating System</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable = no" name="viewport" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="<?php echo $PUBLIC_FILE_URL;?>css/style-frontend.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo $PUBLIC_FILE_URL;?>css/font-awesome.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
</head>

<body>
