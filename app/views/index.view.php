<?php include('include/header.php');
$class  =   '';
if(!isset($lead_id))
    $class  =   'disabled';
?>


<section class="gradient-bg-2">
            <div class="container-fluid">
                <div class="logo">
                    <a href="">
                        <img src="<?php echo $PUBLIC_FILE_URL;?>img/logo2.png" alt="">
                    </a>
                </div>
                <div class="thanku">
                    <div class="thanku-image">
                        <img src="<?php echo $PUBLIC_FILE_URL;?>img/thanku-image.png" alt="">
                        <div class="clearfix"></div>
                    </div>
                    <div class="thanku-mesg">
                        <h3>For rating your bus ride!</h3>
                        <p>We really appreciate you taking time<br/> to share your experience with us.</p>
                    </div>
                </div>
                <div class="form-section">
                    <div class="mainw3-agileinfo">
                        <button class="scrol-down-btn" id="scroll" type="button">scroll<br/> down</button>
                        <div class="registeration-form" id="speak">
                            <div class="register-agileits-top">
                                <form id="register-form" action="" method="post" >
                                    
                                    <input type="hidden" value="<?php echo $lead_id ?>" name="lead_id">
                                    <input type="hidden" value="<?php echo $params[0] ?>" name="bus_no">
                                    <input type="hidden" value="<?php echo $params[1] ?>" name="rating">
                                    <div class="our-field">
                                        <p>Speak Now or Forever Hold Your Peace</p>
                                        <textarea name="comment" class="form-control" style="width:100%; resize:none;"></textarea>
                                    </div>
                                    <div class="our-field">
                                        <p>Name </p>
                                        <input id="input1" type="text" class="name required lettersonly" name="name" >
                                    </div>
                                    <div class="our-field">
                                        <p>Estimated Boarding Time </p>
                                        <select name="hours">
                                            <option value="00">Hour</option>
                                            <option value="12">12</option>
                                            <option value="11">11</option>
                                            <option value="10">10</option>
                                            <option value="9">9</option>
                                            <option value="8">8</option>
                                            <option value="7">7</option>
                                            <option value="6">6</option>
                                            <option value="5">5</option>
                                            <option value="4">4</option>
                                            <option value="3">3</option>
                                            <option value="2">2</option>
                                            <option value="1">1</option>
                                        </select>
                                        <select name="mins" >
                                            <option value="00">Minute</option>
                                            <option value="59">59</option>
                                            <option value="58">58</option>
                                            <option value="57">57</option>
                                            <option value="56">56</option>
                                            <option value="55">55</option>
                                            <option value="54">54</option>
                                            <option value="53">53</option>
                                            <option value="52">52</option>
                                            <option value="51">51</option>
                                            <option value="50">50</option>
                                            <option value="49">49</option>
                                            <option value="48">48</option>
                                            <option value="47">47</option>
                                            <option value="46">46</option>
                                            <option value="45">45</option>
                                            <option value="44">44</option>
                                            <option value="43">43</option>
                                            <option value="42">42</option>
                                            <option value="41">41</option>
                                            <option value="40">40</option>
                                            <option value="39">39</option>
                                            <option value="38">38</option>
                                            <option value="37">37</option>
                                            <option value="36">36</option>
                                            <option value="35">35</option>
                                            <option value="34">34</option>
                                            <option value="33">33</option>
                                            <option value="32">32</option>
                                            <option value="31">31</option>
                                            <option value="30">30</option>
                                            <option value="29">29</option>
                                            <option value="28">28</option>
                                            <option value="27">27</option>
                                            <option value="26">26</option>
                                            <option value="25">25</option>
                                            <option value="24">24</option>
                                            <option value="23">23</option>
                                            <option value="22">22</option>
                                            <option value="21">21</option>
                                            <option value="20">20</option>
                                            <option value="19">19</option>
                                            <option value="18">18</option>
                                            <option value="17">17</option>
                                            <option value="16">16</option>
                                            <option value="15">15</option>
                                            <option value="14">14</option>
                                            <option value="13">13</option>
                                            <option value="12">12</option>
                                            <option value="11">11</option>
                                            <option value="10">10</option>
                                            <option value="9">9</option>
                                            <option value="8">8</option>
                                            <option value="7">7</option>
                                            <option value="6">6</option>
                                            <option value="5">5</option>
                                            <option value="4">4</option>
                                            <option value="3">3</option>
                                            <option value="2">2</option>
                                            <option value="1">1</option>
                                        </select>
                                        <select name="ampm" >
                                            <option value="">AM/PM</option>
                                            <option value="AM">AM</option>
                                            <option value="PM">PM</option>
                                        </select>
                                    </div>
                                    <div class="our-field">
                                        <label class="check">
                                            <input type="checkbox" class="checkbox" name="want_us_to_call_you" value="yes">
                                            <span class="checkbox-span">Want us to contact you? Check this box.</span>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                    <div class="name-phone">
                                        <div class="our-field">
                                            <hr style="border-top: 1px solid #1e4497;" />
                                        </div>                                  
                                        <div class="our-field">
                                            <p>Email</p>
                                            <input id="input2" type="email" class="email" name="email" >
                                        </div>
                                        <div class="our-field">
                                            <p>Phone No.</p>
                                            <input id="input3" type="text" class="phone numbersonly" name="phone" >
                                        </div>
                                    </div>
									<div class="our-field centerdiv">
                                    <input type="submit" value="Submit" <?php echo $class;?>>
                                    
									</div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                 <div class="footer">
                   <div class="copy-right">
                     <p>© 2019 Tower Transit Singapore</p>
                   </div>
                   <div class="social-link">
                     <a href="https://www.facebook.com/towertransitsg/" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                     <a href="https://twitter.com/towertransitSG" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                     <a href="https://www.instagram.com/towertransitsg/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                     <p>@TowerTransitSG</p>
                   </div>
                </div>
            </div>
        </section>
       
<!-- Modal -->
<?php if(!isset($lead_id)) { ?>
<div id="ratingModal" class="modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content modal-bg">
        <div class="modal-header">
        <div class="logo">
          <a href="">
            <img src="<?php echo $PUBLIC_FILE_URL;?>img/logo2.png" alt="">
          </a>
        </div>
      </div>
      <div class="modal-body">
          <div>
              <form id="rating_form" method="post" action="">
                <?php if(!isset($params[0])) { ?>
                    <div>
                        <?php if(isset($Buses) && !empty($Buses)) { ?>
                            <select class="form-control" name="bus_nos" id="bus_nos">
                            <option value="">Select Bus Registration no.</option>
                            <?php foreach($Buses as $bus) { ?>
                                    <option value="<?php echo $bus['registration_no'];?>"><?php echo $bus['registration_no'];?></option>
                            <?php } ?>
                            </select>
                        <?php } ?>
                    </div>
                <?php } ?>
                <div class="rating-heading">
                <?php if(isset($params[0])) { ?>
                <h3>Rate Your Ride</h3>
                <p>We hope you enjoyed your ride! Let us know how it<br/> went by tapping on the appropriate QR code below.</p>
                <?php } ?>
            </div>
                <div id="rating_images" class="rating-section">
                <?php if(isset($params[0])) { ?>
                   
                    <!-- <div class="col-md-6"> -->
                        <div class="qr-code-inner">
                            <div class="qr-images"><a href="<?php echo $BASE_URL;?><?php echo $params[0];?>/1"><img src="<?php echo $PUBLIC_FILE_URL;?>img/love-it-new.png"></a></div>
                            
                       <!--  </div> -->
                    </div>
                    <!-- <div class="col-md-6"> -->
                        <div class="qr-code-inner">
                            <div class="qr-images"><a href="<?php echo $BASE_URL;?><?php echo $params[0];?>/2"><img src="<?php echo $PUBLIC_FILE_URL;?>img/its-ok.png"></a></div>
                            
                        </div>
                    <!-- </div> -->
                    <!-- <div class="col-md-6"> -->
                        <div class="qr-code-inner">
                            <div class="qr-images"><a href="<?php echo $BASE_URL;?><?php echo $params[0];?>/3"><img src="<?php echo $PUBLIC_FILE_URL;?>img/you-can-do-better.png"></a></div>
                            
                        </div>
                    <!-- </div> -->
                <?php } ?>
                </div>
                <!-- <div class="rating-text">
                    <p>(Did you know that our Bus Captains get rewarded<br/> when you give them compliments?)</p>
                </div> -->
                
                </form>
          </div>
         
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>
<?php } ?>

<?php include('include/footer.php'); ?>
<script type="text/javascript">
    $( document ).ready(function() {
        $('#register-form').validate();
        $(".checkbox").on("click",function() {
        $(".name-phone").toggle(this.checked);
        if($(this).prop('checked')){
            $('#input2').addClass('required');
            $('#input3').addClass('required');
        }else{
            $('#input2').removeClass('required');
            $('#input3').removeClass('required');
        }
        });
        $(document).on('keyup','.email',function(){
            var value = $(this).val();
            if(value!=''){
                $('#input3').removeClass('required');
                $('#input3-error').hide();
            }else{
                $('#input3').addClass('required');
                $('#input3-error').show();
            }
        })
        $(document).on('keyup','.phone',function(){
            var value = $(this).val();
            if(value!=''){
                $('#input2').removeClass('required');
                $('#input2-error').hide();
            }else{
                $('#input2').addClass('required');
                $('#input2-error').show();
            }
        })
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^((?:\s*[A-Za-z]\s*){3,})$/i.test(value);
        }, "Please enter letters only");

        jQuery.validator.addMethod("numbersonly", function(value, element) {
            return this.optional(element) || /^([65789][0-9]{7})$/i.test(value);
        }, "Please enter valid phone number");

        $('#ratingModal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $(document).on('change','#bus_nos',function(){
            var bus_no = $(this).val();
            if(bus_no!='' && bus_no!= null){
                $('#rating_images').html('<div class="qr-code-inner"><div class="qr-images" ><a href="<?php echo $BASE_URL;?>'+bus_no+'/1"><img src="<?php echo $PUBLIC_FILE_URL;?>img/love-it-new.png"></a></div></div><div class="qr-code-inner"><div class="qr-images"><a href="<?php echo $BASE_URL;?>'+bus_no+'/2"><img src="<?php echo $PUBLIC_FILE_URL;?>img/its-ok.png"></a></div></div><div class="qr-code-inner"><div class="qr-images"><a href="<?php echo $BASE_URL;?>'+bus_no+'/3"><img src="<?php echo $PUBLIC_FILE_URL;?>img/you-can-do-better.png"></a></div></div>');
                $('.rating-heading').html('<h3>Rate Your Ride</h3><p>We hope you enjoyed your ride! Let us know how it<br/> went by tapping on the appropriate QR code below.</p>');
            }else{
                $('#rating_images').html('');
                $('.rating-heading').html('');
            }

        });
    
        $("#scroll").click(function() {
            $('html, body').animate({
                scrollTop: $("#speak").offset().top
            }, 500);
        });
    });
</script>
