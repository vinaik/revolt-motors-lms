<?php
$router->get(SUB_PATH.'/admin', 'LoginController@index');
$router->post(SUB_PATH.'/admin', 'LoginController@index');
$router->get(SUB_PATH.'/admin/leads','LeadsController@index');
$router->get(SUB_PATH.'/admin/booking-users','BookingsController@index');
$router->get(SUB_PATH.'/admin/logout','UsersController@logoutUser');
$router->get(SUB_PATH.'/admin/downloadLeadscsv','LeadsController@leadsCSV');
$router->post(SUB_PATH.'/admin/allleads','LeadsController@allLeads');
$router->get(SUB_PATH.'/admin/forgotPassword', 'LoginController@forgotPassword');
$router->post(SUB_PATH.'/admin/forgotPassword', 'LoginController@forgotPassword');
$router->get(SUB_PATH.'/admin/resetPassword', 'LoginController@resetPassword');
$router->get(SUB_PATH.'/admin/downloadLink','BookingsController@downloadBitlyLinksCSV');
$router->post(SUB_PATH.'/admin/allBookingsleads','BookingsController@allLeads');
$router->get(SUB_PATH.'/admin/delhi-bookings','BookingsController@delhi_bookings');
$router->get(SUB_PATH.'/admin/pune-bookings','BookingsController@pune_bookings');
$router->get(SUB_PATH.'/admin/total-bookings','BookingsController@total_bookings');
