<?php
namespace App\Library;
use App\Library\PHPMailer;
use App\Library\SMTP;
use App\Library\Exception;
/**
 * PHPMailer class
 */

class Mailer
{
	private $mailInfo = array();

	private function mailing(){

		$mail = new PHPMailer();


		$mail->CharSet="utf-8";
		$mail->IsSMTP();            // set mailer to use SMTP
		$mail->Host = "smtp.sendgrid.net";  // specify main and backup server
		$mail->SMTPAuth = true;         // turn on SMTP authentication
		//$mail->SMTPDebug = 2;
		$mail->Username = 'ravikathait01@gmail.com';
        $mail->Password = '!Q@W3e4r5t';
		$mail->Port = 25;
		$mail->From = $this->mailInfo['from'];
		$mail->FromName = $this->mailInfo['from_name'];
		$mail->AddAddress($this->mailInfo['to'], $this->mailInfo['to_name']);

		$mail->IsHTML(true);         // set email format to HTML (true) or plain text (false)
		$mail->SMTPSecure = 'tls';
		$mail->Subject = $this->mailInfo['subject'];
		$mail->Body    = $this->mailInfo['Message'];
	    // $mail->AltBody = "This is the body in plain text for non-HTML mail clients";
		//$mail->AddEmbeddedImage('images/logo.png', 'logo', 'logo.png');
		//$mail->addAttachment('files/file.xlsx');
		if(!$mail->Send())
		{
		 return json_encode(array('status'=>'error','msg'=>"Message could not be sent. Mailer Error: " . $mail->ErrorInfo));
		}else{
		  return json_encode(array('status'=>'success','msg'=>"Mail has been sent "));

		}
	}
	public function sendMail($array=array()){
		// For demo
		$this->mailInfo['to']=$array['to'];
		$this->mailInfo['to_name']=$array['to_name'];
		$this->mailInfo['from']='tech@armworldwide.com';
		$this->mailInfo['from_name']='Revolt Motors';
		$this->mailInfo['subject']=$array['subject'];
		$this->mailInfo['Message']=$array['message'];
		$response=$this->mailing();
		return json_encode(array('status'=>'success','msg'=>"Mail has been sent "));
		//echo $response; exit;
	}
}
