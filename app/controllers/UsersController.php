<?php

namespace App\Controllers;

use App\Models\Users ;
class UsersController
{
    /**
     * Show all users.
     */
    public function __construct(){

       if(!checkSession('admin'))
       {
           //$db->enableQueryLog();
           redirect('admin');
       }


    }

    public function index()
    {
        /*$user  		=   new Users();
        if($data=checkPostRequest())
        {
            $user   =   $user->insertUser($data);
            redirect('');
        }
        $result     =   $user->getUsers();() */


        return view('admin/user');
    }

    public function UserCSV(){
        $user               =   new Users();
        $result       		=   $user->getUsersCSV();
        $filename           =   'WTCUserData-'.date('d/m/Y');

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$filename.'.csv');

        $output             =   fopen('php://output', 'w');
        $names              =   array('Name','Email','Phone');
        $count              =   0;

        fputcsv($output,$names);

        if(isset($result) && !empty($result)){
            foreach($result as $data){
                $modifiedData=array($data['name'],$data['email'],$data['phone'],$data['utm_source'],$data['utm_medium'],$data['utm_term']);
                fputcsv($output,$modifiedData);
            }
        }
    }
    public function logoutUser(){
      session_destroy();
      redirect('admin');
    }

}
