<?php

namespace App\Controllers;
use App\Models\Bookings;
use ZipArchive;
class BookingsController
{

  public function __construct(){

    if(!checkSession('admin'))
    {
      redirect('admin');
    }


  }

  public function index()
  {

    return view('admin/booking',['buses'=>array()]);
  }
  public function allLeads()
  {
    $bus               =   new Bookings();
    $result              =   $bus->getBuses();
    echo json_encode($result);
  }
  public function delhi_bookings() {
    $lead               =   new Bookings();
    $result       		=   $lead->getDelhiBooking();
    $filename           =   'Delhi-bookings-'.date('d/m/Y');

    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename='.$filename.'.csv');

    $output             =   fopen('php://output', 'w');
    $names              =   array('Name','Mobile','Email','City','Bike Color','Order ID','Order Status');
    $count              =   0;

    fputcsv($output,$names);

    if(isset($result) && !empty($result)){
      foreach($result as $data){
        if($data['order']['order_status'] =='done'){
          $modifiedData=array($data['name'],$data['mobile'],$data['email'],$data['city'],$data['bike_color'],$data['order']['razorpay_order_id'],$data['order']['order_status']);
          fputcsv($output,$modifiedData);
        }

      }
    }

  }

  // Pune booking
  public function pune_bookings() {
    $lead               =   new Bookings();
    $result       		=   $lead->getPuneBooking();
    $filename           =   'Pune-bookings-'.date('d/m/Y');

    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename='.$filename.'.csv');

    $output             =   fopen('php://output', 'w');
    $names              =   array('Name','Mobile','Email','City','Bike Color','Order ID','Order Status');
    $count              =   0;

    fputcsv($output,$names);
    if(isset($result) && !empty($result)){
      foreach($result as $data){
        if($data['order']['order_status'] =='done'){
          $modifiedData=array($data['name'],$data['mobile'],$data['email'],$data['city'],$data['bike_color'],$data['order']['razorpay_order_id'],$data['order']['order_status']);
          fputcsv($output,$modifiedData);
        }

      }
    }

  }

  // total booking
  public function total_bookings() {

    $lead               =   new Bookings();
    $result       		=   $lead->getTotalBooking($_REQUEST);
    $filename           =   'Totoal-bookings-'.date('d/m/Y');

    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename='.$filename.'.csv');

    $output             =   fopen('php://output', 'w');
    if($_REQUEST['rv_code'] =='rv-registration'){
      $names              =   array('Name','Mobile','Email','City','Bike Color','Coupon');
    }else{
      $names              =   array('Name','Mobile','Email','City','Bike Color','Order ID','Order Status');
    }

    $count              =   0;

    fputcsv($output,$names);
    if(isset($result) && !empty($result)){
      foreach($result as $data){
        if(isset($data['order']['order_status']) && $data['order']['order_status'] =='done' && $_REQUEST['rv_code'] !='rv-registration'){
          $modifiedData=array($data['name'],$data['mobile'],$data['email'],$data['city'],$data['bike_color'],$data['order']['razorpay_order_id'],$data['order']['order_status']);
          fputcsv($output,$modifiedData);
        }
        if($_REQUEST['rv_code'] =='rv-registration'){
          $modifiedData=array($data['name'],$data['mobile'],$data['email'],$data['city'],$data['bike_color'],$data['coupon']);
          fputcsv($output,$modifiedData);
        }

      }
    }

  }

}
