<?php
namespace App\Controllers;
use App\Models\Users;
use App\Models\Admin;
use App\Library\Mailer;
class LoginController
{
  /**
  * Show the home page.
  */
  public function __construct()
  {

  }

  public function index()
  {
    if($data=checkPostRequest())
    {
      $result = Admin::whereemail($data['username'])->wherePassword(md5($data['password']))->get()->toArray();
      if(count($result)>0)
      {
        setSession('admin',$result);
        redirect('admin/leads');
      }
      return view('admin/login', ['msg' => 'Invalid Credential']);
    }
    return view('admin/login');
  }

  public function forgotPassword()
  {
    if($data=checkPostRequest())
    {
      $result = Admin::whereemail($data['username'])->get()->toArray();
      if(count($result)>0)
      {
        $mailer = new Mailer();
        $message='<p>Dear '.$result[0]['email'].'</p>';
        $message.='<p>You (or someone else) requested to change the password used to access '.BASE_URL.'. </p>';
        $message.='<p>
        Please go to the following link to reset your password: </p><p><a href="'.BASE_URL.'admin/resetPassword/'.$result[0]['user_id'].'">Click here to reset your password. </a></p>';
        $message.='<p>Regards,</p>';
        $message.='<p>Revolt Motors</p>';
        $mailer->sendMail(array('to'=>$result[0]['email'],'to_name'=>$result[0]['email'],'subject'=>'Reset Password','message'=>$message));
        return view('admin/forgotpassword', ['msg' => 'The reset password email sent to your inbox.','color'=>'green']);
      }
      return view('admin/forgotpassword', ['msg' => 'This email does not exist!','color'=>'red']);
    }
    return view('admin/forgotpassword');
  }

  public function resetPassword($param=array())
  {
    if($data=checkPostRequest())
    {
      $admin  = new Admin;
      $result =  $admin->updateInfo($data);
      if($result)
      {
        return redirect('admin/login');
      }
    }
    return view('admin/resetpassword',['id'=>$param[0]]);
  }

}
