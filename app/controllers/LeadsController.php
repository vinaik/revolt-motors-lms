<?php
namespace App\Controllers;
use App\Models\Leads ;
class LeadsController
{
  /**
  * Show all users.
  */
  public function __construct(){

    if(!checkSession('admin'))
    {
      //$db->enableQueryLog();
      redirect('admin');
    }
  }

  public function index()
  {
    return view('admin/user',['leads'=>'']);
  }

  // fetch all leads
  public function allLeads()
  {
    $lead               =   new Leads();
    $result               =   $lead->getLeads();
    echo json_encode($result);
  }

  // Download CSV for unpaid users 
  public function leadsCSV(){
    $lead               =   new Leads();
    $result       		=   $lead->getLeadsCSV();
    $filename           =   'Unpaid User List-'.date('d/m/Y');

    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename='.$filename.'.csv');

    $output             =   fopen('php://output', 'w');
    $names              =   array('Name','Mobile','Email', 'City', 'Register Date');
    $count              =   0;

    fputcsv($output,$names);

    if(isset($result) && !empty($result)){
      foreach($result as $data){
        $modifiedData=array($data['name'],$data['mobile'],$data['email'],$data['city'],$data['register_date']);
        fputcsv($output,$modifiedData);
      }
    }
  }

}
