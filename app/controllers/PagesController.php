<?php

namespace App\Controllers;
use App\Models\Leads ;
use App\Models\Buses ;
use App\Models\TTS ;
class PagesController
{
  public $rating=array('1'=>'LOVE IT','2'=>'IT`S OKAY LAH','3'=>'YOU CAN DO BETTER');

  public function __construct(){



  }

  public function index($param=array())
  {

    if($data=checkPostRequest())
    {
      $rating=array('1'=>'LOVE IT','2'=>'IT`S OKAY LAH','3'=>'YOU CAN DO BETTER');
      /* Zendesk Credetials */
      $BASEPATH = parse_ini_file(ROOT_PATH."/config.ini",true);
      $zendesk_domain = $BASEPATH['zendesk']['domain'];
      $zendesk_useremail   =   $BASEPATH['zendesk']['useremail'];
      $zendesk_token   =   $BASEPATH['zendesk']['token'];

      // echo base64_encode($zendesk_useremail."/token:".$zendesk_token);die;
      $d  =   date("Y-m-d");
      $data['boarding_datetime'] = date("Y-m-d H:i:s", strtotime($d." ".$data['hours'].":".$data['mins']));
      //echo $data['boarding_datetime'] = date("Y-m-d H:i:s", strtotime("2019-04-23 16:03:00"));
      $tts       =   new TTS();
      $captainDetail   =   $tts->getCaptainNo($data);
      $data['driverno']   =   "Not Found!";
      if(isset($captainDetail) && !empty($captainDetail)){
        $data['driverno']   =   $captainDetail[0]['driverno'];
      }

      $ticket['ticket']  = [
        "subject"=>"Tower transit lead",
        "description"=> "Bus No. ".$data['bus_no']."Captain No. ".$data['driverno']."Rating ".$rating[$data['rating']]." Name: ".$data['name']." Email: ".$data['email']." Phone: ".$data['phone']." Comment: ".$data['comment']." Boarding Time: ".$data['hours'].":".$data['mins']." ".$data['ampm']
      ];
      /* Initiate CURL request */

      $ch = curl_init();
      $curlConfig = array(
        CURLOPT_URL            => $zendesk_domain,
        CURLOPT_HTTPHEADER     => array('Content-Type: application/json','Authorization: Basic c2hhcmlmYWguYWxzYWdvZmZAdG93ZXJ0cmFuc2l0LnNnL3Rva2VuOlVDSTU5cmRXU3lzMXNzSlhacXd0T29KeVRRZVZnTnJTSEJUaWRadTI='),
        CURLOPT_POST           => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POSTFIELDS     => json_encode($ticket)
      );
      curl_setopt_array($ch, $curlConfig);
      $result = curl_exec($ch);
      curl_close($ch);

      /* UPDATE API RESPONSE */
      $data['api_response']   =   $result;
      $lead       =   new Leads();
      $userdata   =   $lead->updateLead($data);

      redirect('thankyou');
    }
    if(isset($param) && !empty($param[0]) && !empty($param[1]) && $param[0]!='public' && $param[1]!='image'){
      $lead   =   new Leads();
      $data   =   array();
      $data['bus_id']     =   $param[0];
      $data['rating']     =   $param[1];
      $lead_id  =   $lead->insertLead($data);
      return view('index',['lead_id'=>$lead_id,'params'=>$param]);
    }
    $bus               =   new Buses();
    $buses              =   $bus->getAllBuses();
    return view('index',['params'=>$param,'Buses'=>$buses]);

  }

  public function thankyou()
  {
    return view('thankyou',[]);
  }
}
