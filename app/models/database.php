<?php
namespace App\Models; 
use Illuminate\Database\Capsule\Manager as Capsule;
 
class Database {
 
    function __construct() {
            $this->settings = parse_ini_file(ROOT_PATH."/config.ini",true);
            $capsule        = new Capsule;
        
            $capsule->addConnection([
                'driver'   => 'mysql',
                'host'     => $this->settings['database']['host'],
                'database' => $this->settings['database']['name'],
                'username' => $this->settings['database']['user'],
                'password' => $this->settings['database']['pwd'],
                'charset'  => 'utf8',
                'collation'=> 'utf8_unicode_ci',
                /*'driver'   => 'mysql',
                'host'     => 'localhost',
                'database' => 'oppo_quiz',
                'username' => 'root',
                'password' => 'Mysql@1234$#$',
                
                'collation'=> 'utf8_unicode_ci',
                'prefix'   => '',*/
               
            ]);
        
            $capsule->bootEloquent();
    }
 
}