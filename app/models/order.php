<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class Order extends Model {
    protected $table        =   'orders';
    protected $fillable     =   ['id','user_id','razorpay_order_id'];
    public $timestamps      =   false;
    public $data;

    public function user()
    {
        return $this->hasOne('App\Models\Buses');
    }

}
