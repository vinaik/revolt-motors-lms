<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;
use App\Models\Users ;

class Leads extends Model {
  protected $table        =   'users';
  protected $fillable     =   ['id'];
  public $timestamps      =   false;
  public $perPageLimit    =   10;
  public $totalCount      =   0;
  public $pages           =   5; // Pages links per page
  public $previouspages   =   2;
  public $nextpages       =   2;

  public function getLeads()
  {
    $_SESSION['SEARCH'] =   array();

    $this->q = Users::select('*');

    if(isset($_REQUEST['text_search']) && !empty($_REQUEST['text_search'])){
      $_SESSION['SEARCH']['text_search']  =   $_REQUEST['text_search'];
      $this->q = $this->q->where('mobile', '=', $_REQUEST['text_search']);

    }
    $this->totalCount   =   $this->q->count();
    $this->setLimit();
    $this->recordOrderBy();
    $this->q = $this->q->get();
    $res[]   =   $this->q->toArray();
    $result =    $res[0];
    $result['pagination']   =    $this->getPagination();
    return $result;
  }
  private function recordOrderBy()
  {
    $this->q->orderBy('id','DESC');
  }

  private function setSearch()
  {
    if(isset($_SESSION['SEARCH']['text_search']) && !empty($_SESSION['SEARCH']['text_search'])){
      $this->q = $this->q->where('bus_id', '=', $_SESSION['SEARCH']['text_search']);

    }
    if(isset($_SESSION['SEARCH']['rating']) && !empty($_SESSION['SEARCH']['rating'])){
      $this->q = $this->q->where('rating', '=', $_SESSION['SEARCH']['rating']);
    }
    if(isset($_SESSION['SEARCH']['start']) && !empty($_SESSION['SEARCH']['end'])){

      $this->q = $this->q->whereBetween('created_at',[$_SESSION['SEARCH']['start'],$_SESSION['SEARCH']['end']]);
    }


  }
  private function setLimit()
  {
    if(isset($_REQUEST['page']) && !empty($_REQUEST['page']))
    {
      $start  =   $_REQUEST['page']*$this->perPageLimit;
    }
    else
    {
      $start=0;
    }
    if($_REQUEST['page']==1){
      $start=0;
    }
    $this->q->skip($start)->take($this->perPageLimit);
  }
  private function getPagination(){
    $start          =   isset($_REQUEST['page'])?$_REQUEST['page']:1;
    $totalCount     =   $this->totalCount;
    $totalPages     =   floor($totalCount/$this->perPageLimit);
    $pagination     =   array();

    if($start==1){
      $prev  =   $start;
      $next  =   $start   +   $this->pages - 1;

    }elseif($start == $totalPages){
      $prev  =   $start   -   $this->previouspages;
      $next  =   $start   +   $this->nextpages;

    }else{
      $prev  =   $start   -   $this->previouspages;
      $next  =   $start   +   $this->nextpages;
    }

    if($prev <= 0)
    $prev   =   1;
    if($next > $totalPages)
    $next   =   $totalPages;

    for($i = $prev; $i <= $next; $i++){
      $pagination[] =   $i;
    }

    if($next <  $totalPages-1){
      $pagination[] =   "break";

      $pagination[] =   $totalPages-1;

    }

    if($next <  $totalPages)
    $pagination[] =   $totalPages;

    return $pagination;

  }

  public function getLeadsCSV()
  {

    $this->q = Leads::select('*');
    //$this->setSearch();
    $this->q = $this->q->get();
    $res[]   =   $this->q->toArray();
    $result =    $res[0];
    return $result;
  }
}
