<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;
use App\Models\Users ;
use App\Models\Order ;

class Bookings extends Model {
    protected $table        =   'booking_users';
    protected $fillable     =   ['id'];
    public $timestamps      =   false;
    public $perPageLimit    =   10;
    public $totalCount      =   0;
    public $pages           =   5; // Pages links per page
    public $previouspages   =   2;
    public $nextpages       =   2;
    public function order()
    {
        return $this->hasOne('App\Models\Order','user_id');
    }

    public function leads()
    {
        return $this->hasMany('App\Models\Leads','registration_no','bus_id');
    }


    public function checkBusbyRegid($regid)
    {
        $bus  =   $this->whereregistration_no($regid)->first();

        if($bus)
        {
            $busresult          =       $bus->getOriginal();
            return false;
        }
        return  true;
    }
    private function curl_get_result($url){
        $ch      = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return trim($data);
    }

    public function getBuses($limit=10)
    {
        $result = array();
        $this->q            =  $this->select('*');
        $this->setSearch($_REQUEST);
        $this->totalCount   =   $this->q->count();
        $this->setLimit();
        $this->recordOrderBy();

        $this->q = $this->q->get();
        $res[]   =   $this->q->toArray();
        $result =    $res[0];
        $result['pagination']   =    $this->getPagination();
        return $result;
    }
    private function recordOrderBy()
    {
        $this->q->orderBy('booking_users.id','DESC');
    }

    private function setSearch()
    {
        if(isset($_REQUEST['text_search']) && !empty($_REQUEST['text_search'])){
            $this->q = $this->q->where('mobile', '=', $_REQUEST['text_search']);

        }
    }
    private function setLimit()
    {
        if(isset($_REQUEST['page']) && !empty($_REQUEST['page']))
        { $start  = 0;
            if($_REQUEST['page']>1)
            {
            $start  =  ( $_REQUEST['page']-1)*$this->perPageLimit;
            }
        }
        else
        {
            $start=0;
        }
        $this->q->skip($start)->take($this->perPageLimit);
    }


    private function getPagination(){
        $start          =   isset($_REQUEST['page'])?$_REQUEST['page']:1;
        $totalCount     =   $this->totalCount;
        $totalPages     =   floor($totalCount/$this->perPageLimit);
        $pagination     =   array();

        if($start==1){
            $prev  =   $start;
            $next  =   $start   +   $this->pages - 1;

        }elseif($start == $totalPages){
            $prev  =   $start   -   $this->previouspages;
            $next  =   $start   +   $this->nextpages;

        }else{
            $prev  =   $start   -   $this->previouspages;
            $next  =   $start   +   $this->nextpages;
        }

        if($prev <= 0)
            $prev   =   1;
        if($next > $totalPages)
            $next   =   $totalPages;

        for($i = $prev; $i <= $next; $i++){
            $pagination[] =   $i;
        }

        if($next <  $totalPages-1){
            $pagination[] =   "break";

            $pagination[] =   $totalPages-1;

        }

        if($next <  $totalPages)
            $pagination[] =   $totalPages;

        return $pagination;

    }

    public function updateBusStatus($id,$status){

        $bus = $this->find($id);
        if($status == 1)
            $bus->deleted_at = '0';
        else
        $bus->deleted_at = '1';

        $bus->save();
        return true;

    }
    public function getLeadsCSV()
    {

        $this->q = Buses::select('*');
        $this->q = $this->q->get();
        $res[]   =   $this->q->toArray();
        $result =    $res[0];
        return $result;
    }


    public function getDelhiBooking()
    {

        $this->q = $this->with(['order'=>function($join){
            $join->where('orders.order_status','=','done');
        }]
      );
        $this->q = $this->q->where('booking_users.city','delhi');
        $this->q = $this->q->get();
        $res[]   =   $this->q->toArray();
        $result =    $res[0];
        return $result;
    }

    public function getPuneBooking()
    {

        $this->q = $this->with(['order'=>function($join){
            $join->where('orders.order_status','=','done');
        }]
      );
        $this->q = $this->q->where('booking_users.city','pune');
        $this->q = $this->q->get();
        $res[]   =   $this->q->toArray();
        $result =    $res[0];
        return $result;
    }

  public function getTotalBooking($data =array())
  {
    if(isset($data['city']) && $data['city']=='other'){
      $this->q = $this->q->where('booking_users.city','');
      $this->q = $this->q->get();
      $res[]   =   $this->q->toArray();
      $result =    $res[0];
      return $result;
    }

    if(!empty($data['city']) && $data['city'] !='other'){
      // if bike color selected with city
      if(!empty($data['bike_color'])){

          $this->q = $this->with(['order'=>function($join){
            $join->where('orders.order_status','=','done');
          }]
        );
        $this->q = $this->q->where([
          ['booking_users.city','=',$data['city']],
          ['booking_users.bike_color','=',$data['bike_color']],
        ]);
        $this->q = $this->q->get();
        $res[]   =   $this->q->toArray();
        $result =    $res[0];
        return $result;

      }
        // if city is selected and bike color not selected
        $this->q = $this->with(['order'=>function($join){
          $join->where('orders.order_status','=','done');
        }]
      );
      $this->q = $this->q->where('booking_users.city',$data['city']);
      $this->q = $this->q->get();
      $res[]   =   $this->q->toArray();
      $result =    $res[0];
      return $result;
    }

    // if bike color selected and city not selected

    if(empty($data['city']) && !empty($data['bike_color'])){
      $this->q = $this->with(['order'=>function($join){
        $join->where('orders.order_status','=','done');
      }]
    );
    $this->q = $this->q->where('booking_users.bike_color',$data['bike_color']);
    $this->q = $this->q->get();
    $res[]   =   $this->q->toArray();
    $result =    $res[0];
    return $result;
    }

    // bike color and city empty
    if(empty($data['bike_color']) && empty($data['city']) && empty($data['rv_code'])){
      $this->q = $this->with(['order'=>function($join){
        $join->where('orders.order_status','=','done');
      }]
    );
    $this->q = $this->q->get();
    $res[]   =   $this->q->toArray();
    $result =    $res[0];
    return $result;
    }

    // registration with RV code

    if(!empty($data['rv_code'])){
        if($data['rv_code'] == 'rv-registration'){
          $this->q = $this->whereNotNull('booking_users.coupon');
          $this->q = $this->q->get();
          $res[]   =   $this->q->toArray();
          $result =    $res[0];
          return $result;
        }
        if($data['rv_code'] == 'rv-payment'){
          $this->q = $this->with(['order'=>function($join){
            $join->where('orders.order_status','=','done');
          }]);
          $this->q = $this->q->where('booking_users.coupon','!=','');
          $this->q = $this->q->get();
          $res[]   =   $this->q->toArray();
          $result =    $res[0];
          return $result;
        }
    }

  }

}
