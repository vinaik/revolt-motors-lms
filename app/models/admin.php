<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class Admin extends Model {
    protected $table = 'admin';
    protected $fillable     =   ['email','password'];
    public $timestamps      =   false;
    protected  $primaryKey = 'user_id';

    public function updateInfo($data){
        $admin = $this->find($data['user_id']);
        $admin->password = md5($data['password']);
        $admin->save();
        return true;
    }


}
