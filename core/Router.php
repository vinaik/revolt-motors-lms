<?php

namespace App\Core;
use \Exception ;
class Router
{
    /**
     * All registered routes.
     *
     * @var array
     */
    public $routes = [
        'GET' => [],
        'POST' => []
    ];

    /**
     * Load a user's routes file.
     *
     * @param string $file
     */
    public static function load($file)
    {
        $router = new static;

        require $file;

        return $router;
    }

    /**
     * Register a GET route.
     *
     * @param string $uri
     * @param string $controller
     */
    public function get($uri, $controller)
    {
        $this->routes['GET'][$uri] = $controller;
    }

    /**
     * Register a POST route.
     *
     * @param string $uri
     * @param string $controller
     */
    public function post($uri, $controller)
    {
        $this->routes['POST'][$uri] = $controller;
    }

    /**
     * Load the requested URI's associated controller method.
     *
     * @param string $uri
     * @param string $requestType
     */
    public function direct($uri, $requestType)
    {
        $urlelement=explode('/',$uri);
        if (array_key_exists($uri, $this->routes[$requestType])) {
            return $this->callAction(
                ...explode('@', $this->routes[$requestType][$uri])
            );
        }
        else
        {
            $str='';
            $params=array();
            foreach($urlelement as $key=>$value)
            {
               $str.=$value;
               if(array_key_exists($str, $this->routes[$requestType])) 
               {
                   $existingurl=$str;
                   $params= array_slice($urlelement,$key+1);
                   
               }
                $str.='/';
            }
            if($existingurl)
            {
                $rount= explode('@',$this->routes[$requestType][$existingurl]);
                if (array_key_exists($existingurl, $this->routes[$requestType])) {
                 return $this->callAction($rount[0],$rount[1],$params);
               }
            }
        }

        throw new Exception('No route defined for this URI.');
    }

    /**
     * Load and call the relevant controller action.
     *
     * @param string $controller
     * @param string $action
     */
    protected function callAction($controller, $action,$params=array())
    {
     
        $controller = "App\\Controllers\\{$controller}";
        $controller = new $controller;

        if (! method_exists($controller, $action)) {
            throw new Exception(
                "{$controller} does not respond to the {$action} action."
            );
        }

        return $controller->$action($params);
    }
}
